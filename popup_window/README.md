# Popup Checkout Window #

Adding a javascript popup window to your existing shopping cart process is the easiest/quickest way to integrate BitRail as a payment checkout option.

The checkout window accepts the order details and then opens a window which completes the process through BitRail. If the user isn't already logged into BitRail, the window prompts them for login information.

## Implementation overview ##

The popup window is comprised of both a server-side component and a front-end UI side. **The server-side call is absolutely required for security, and cannot be skipped.**

The server-side component is simply a token generation used by the front end. The front end popup is kicked off by use a BitRail-supplied javascript. Lastly, the resulting value can be verified if required again by the server-side code, to ensure validity.


![](/popup_window/order_popup_flow.png)


## Server side authentication

A JWT token / `access_token` **must be** generated server-side and passed to the front-end UI to ensure the *clientID* and *secret* used are not exposed to the public. 

The [client credentials guide](?file=/oauth/client_credentials.md) details a straightforward implementation.

[A full guide on generating this `access_token` can be found in the [OAUTH guide](?file=/oauth/README.md).]

The `access_token` can be safely passed to the front end UI (specified by `order_popup_jwt`) in the order below.


## Javascript ##

Note: This guide steps through each part with more detail, but if you would like to download a basic, but working example can be [downloaded here](/popup_window/order_popup_example.html).

The front-end UI can be any client type; however, to make integration faster and easier to implement, BitRail offers a javascript library which can be linked directly from our CDN:

`https://cdn.bitrail.io/js/bitrail-vendor.js
`

### Initialization ###

Two main calls are needed for the checkout window. The `BitRail.init()` call initializes the library with the JWT/access_token generated above, and array with the BitRail API URL, the javascript element ID to show the window, and an optional array with style elements:


        BitRail.init(order_popup_jwt,
        {
            api_url: BITRAIL_API_URL,
            // where the UI will go
            parent_element: document.getElementById(iframeid),
            // Any additional style elements- force a width, height, etc
            frame_attributes: {style: null}
        });

The BitRail API URL will likely be one of:

* https://api.sandbox.bitrail.io - Sandbox environment
* https://api.bitrail.io - Production environment

### Show the order window ###

To generate the order checkout window, a call to `BitRail.order()` is made. 

In order for the system to know where to send the funds, the `sellerVaultHandle` must be specified. Each vault in the BitRail system can be identified by a vault handle, and is safe for public visibility (and is intended as such). The vault handle represents the user, business, or storefront vault that should receive the funds.


The order call takes a number of parameters that should be the data for this order:

* `orderUniqueToken`: Any token unique to this order. This token is not stored by BitRail. Rather, it is used for validating the transaction (see below)
* `sellerVaultHandle`: This field is the recipient of this order.
* `amount`: This is a numeric (numbers and decimals) value of the total to be transacted
* `currency`: Either "USD" or "FC"
* `orderDescription`: Any alphanumeric description for this transaction. Description may be up to 255 characters.
* `orderAttributes`: Ad-hoc attributes for seller and buyer to see. These may be any key/value pairs, and will be associated with the transaction. Up to 100 attributes allowed. Key length and value length each must be under 255 characters. Keys may not start with an underscore.          
* `order_callback`: A javascript callback function to be called with the result of the window (closed, success, failure, etc).

Example call:

        BitRail.order(orderUniqueToken,          // unique ID for this transaction
            "seller2345",                        // seller's vault handle
            521.24,                              // amount. Ex:  521.24
            "USD",                               // currency 
            "Some order example",                // order description
            {
            // List of order attributes
                "order_id": "45634588",
                "item_id_1": "34435 - Small rocket",
                "item_price_1": "1.20",
                "tax": "0.14"
            }, 
            order_callback                       // completion callback
            );
            

### Callback ###

When the modal window closes, the `order_callback` function is called with a `response` parameter. 

    function order_callback(response) {
        switch (response.status) {
                case "success":
                    // order submitted successfully
                    break;
                case "failed":
                    // order was not able to complete
                    break;
                case "cancelled":
                    // checkout process was cancelled by the user
                    break;
            }
    }
    
#### Success ####

If the `response.status` is "success" then the checkout was successful. The `resp` object will include:

	{
      status: 'success',
      dest_vault_handle: destinationValueHandle,
      fc_transaction_id: transactionID,
      verification_token: verificationToken
    }
    
The `fc_transaction_id` field contains the BitRail transaction ID for this transaction.

The `verification_token` field contains an encrypted packet to be sent back to the server. See the [Javascript verification](#js-verification) for more information.


#### Failure ####

If the `callback.status` is "failure", then the order was unable to complete successfully. This could be due to the destination (seller) vault being in a inactive status, insufficient funds to process the transaction, or other operational issue.


#### Cancelled ####

If the `callback.status` is "cancelled", then the order was cancelled by the user before the checkout was completed. No other data is returned in this scenario.

## Transaction Verification ##

Because the order process is started and finished via javascript, it's possible for a malicious user to intercept the values and status being sent back; they could mark an order as successful/paid, when in fact its actually not. In order to ensure that the order was actually successfully processed, the transaction should be validated. 

The `verificationToken` is an encrypted json data packet, sent back in the callback upon success. This token should be passed back to the server, where decryption takes place.

The encrypted packet is in the form of `methodName:param;message`. The current methodName is `AES-CBC-PKCS7` and the parameter is the initialization vector.

The packet, once decrypted, contains:

* `rnd`: random string
* `order_token`: original order token passed in
* `amount`: amount paid
* `fc_transaction_id`: the transaction id of the order created. 

If the original `order_token` matches the `orderUniqueToken` passed in when the process started, then the transaction is valid and successful. 

### Example decryption code
Example decrpytion code can be downloaded:

* [node.js example](/popup_window/verification_example_node.js)
* [PHP example](/popup_window/verification_example.php)


### API Transaction Lookup ###

The transaction details can be retrieved via an API call on the server, using the same server-side JWT as discussed above in the **Server-side token generation** section.

An example cURL call:

	curl --location -g --request GET '{{BITRAIL_API_URL}}/v1/transactions/{{TRANSACTION_ID}}' \
		--header 'Content-Type: application/x-www-form-urlencoded' \
		--header 'Authorization: Bearer {{access_token}}' \
		--header 'Accept: application/json'

This call will return the details of the transaction from the BitRail system, including the order description and any attributes specified.


## Working UI Example ##

A working example of the UI can be [downloaded here](/popup_window/order_popup_example.html).

Please note that you will still need to implement the `auth` call as described above in the **Server-side token generation** section.

// NODEJS SAMPLE
//
// NOTE: This sample only works on the older token format. See the PHP sample to see how the new format is verified!!
//
'use strict';

const crypto = require('crypto');

// PRIVATE KEY -- This is the sandbox key. Production clients will have their own
//             -- Production key should be protected so no unauthorized persons may ever see it
//             -- For example, do not place in source code but load from environment
var key = 'sDGTZrkmvRipesnKEEmSRQ==';
const ENC_KEY = Buffer.from(key, 'base64');

// encToken would be the encrypted verificationToken returned from making an order
const encToken = "AES-CBC-PKCS7:fYFyEdNuk7+OSaNRIBoPjQ==;AY2JAicwqVjqssT1s1dJTgB/2gom0T/22DRidC/waeasKDlFFDu3DxL+gFNFswTik2lBKaBqXT5aM/bwH6jZAhwLFfsVgOblkI/KPZSMh7xIq5NQVRBPsuEBqxPidrMXTWeY4D+Gjeg2CBDWbl+POw==";

if (!encToken.startsWith('AES-CBC-PKCS7')) {
    // ERROR HERE
} else {
    const phrase = encToken.split(';')[1];
    const iv = Buffer.from( encToken.split(';')[0].split(':')[1] ,'base64');

    var decrypt = ((msg, iv) => {
        let decipher = crypto.createDecipheriv('aes-128-cbc', ENC_KEY, iv);
        let decrypted = decipher.update(msg, 'base64', 'utf8');
        return (decrypted + decipher.final('utf8'))
    });

    let val = decrypt(phrase, iv);
    console.log(val);

    let orderValues = JSON.parse(val);

    // decoded into orderValues array. EX: {"amount":"987.65","fc_transaction_id":"20ei35inggf38kqgqoxh","order_token":"12345678","rnd":"6tqeilcp"}
}
<?php

// SHARED KEY  -- This is the sandbox key. Production clients will have their own
//             -- Production key should be protected so no unauthorized persons may ever see it
//             -- The CLIENT_ID, CLIENT_SECRET should also come from the environment or secure location and never exposed
//             -- The JWT would come from a process, see the docs on authentication.
//             -- For example, do not place in source code but load from environment
$key = "sDGTZrkmvRipesnKEEmSRQ==";
$ENC_KEY = base64_decode($key, true);

$API_URL = "https://api.qa.bitrail.io/v1/"; // TODO: use the API url supplied by BitRail
$API_CLIENT_ID = ""; // TODO: use the CLIENT_ID and secret supplied by BitRail
$API_CLIENT_SECRET = "";
$JWT = ""; // TODO: login into the API and supply your JWT here

// ---------- END CONFIGURATION -----------

// encToken would be the encrypted verificationToken returned from making an order
$encTokens = ["AES-CBC-PKCS7:fYFyEdNuk7+OSaNRIBoPjQ==;AY2JAicwqVjqssT1s1dJTgB/2gom0T/22DRidC/waeasKDlFFDu3DxL+gFNFswTik2lBKaBqXT5aM/bwH6jZAhwLFfsVgOblkI/KPZSMh7xIq5NQVRBPsuEBqxPidrMXTWeY4D+Gjeg2CBDWbl+POw=="
            ,"RSA-SHA384:ia8VCs8XDRLOMyE+dYtX3GJdcv/Kb/YYDSGSzT2d8BMXXuq9EQDx8GhPX/4SDh1F4vihb51R3rbt3fI1Xxb9VRfqPPgR9DXqBID6ZHZYqmuTM4sLebRFsqyp4Gqc7Mmg+jOmoG4a+3tMDvyLypMV9xxqmAa0k+1dZnNLyD+hDKlArtSp5qzOFe94b4ABq1Ipe9ZYcSe7D/ElYJdUx7knu0sDB5Itj31ZYp7ZLlZr+MydJoXVhVKiyRAfEs2DNheKZ2GnfFOvlRyjeeWnjjmutJSDMwzxEp53mGpFH0a40UQokPD7XaS9G7fABMuCkfFXHM46deprtzdS3zQskt16AA==;eyJhbW91bnQiOiIxMi43NiIsImZjX3RyYW5zYWN0aW9uX2lkIjoiMzFaSEV1MzdlN2QzajAwYzRmeTYiLCJvcmRlcl90b2tlbiI6IjRjdWhIRkRGNTVoZGhlMzNIREBkXHUwMDI2OTMzamNuIn0K"
            ];

foreach ($encTokens as $token) {
	global $JWT;

	// There are two approaches to verifying tokens
	// The first is locally
    switch (explode(":", $token)[0]) {
		case "AES-CBC-PKCS7": verify_encrypted($token); break;
		case "RSA-SHA256": verify_signed($token, OPENSSL_ALGO_SHA256); break;
		case "RSA-SHA384": verify_signed($token, OPENSSL_ALGO_SHA384); break;
		case "RSA-SHA512": verify_signed($token, OPENSSL_ALGO_SHA512); break;
		default: print("Unknown token format \n");
    }

	// The second approach is calling the API and letting it do all the work and returning failure or the embedded data on success
	$resp = callAPI("GET", "transactions/verify", array("token" => $token), $JWT);
	if ($resp === false) {
		print("Error calling verify \n");
		print_r($resp);
	} else {
		print("Token is valid, look at data to confirm if it belongs to your transaction \n");
		print_r($resp);
	}

}

function verify_encrypted($token) {
	global $ENC_KEY;

	print("Verifying encrypted \n");

	if (substr($token, 0, 13) != "AES-CBC-PKCS7") {
		print("Invalid crypted token header \n");
	}

	list($header, $msg) = explode(";", $token);
	list($unused, $iv) = explode(":", $header);

	$iv = base64_decode($iv);

	$method = "AES-128-CBC";
	$blockSize = 128 / 8;
	$plain = openssl_decrypt($msg, $method, $ENC_KEY, OPENSSL_ZERO_PADDING, $iv);
	$plainLen = strlen($plain);
	if ($plainLen > $blockSize) {
		$paddingLen = (int)(ord($plain[$plainLen - 1]));
		$plain = substr($plain, 0, $plainLen - $paddingLen - 1);
	}

	print_r($plain);
	print("\n");

	$orderValues = json_decode($plain);

	// decoded into $orderValues array.
	// EX: {"amount":"987.65","fc_transaction_id":"20ei35inggf38kqgqoxh","order_token":"12345678","rnd":"6tqeilcp"}
}

function verify_signed($token, $algo) {
	print("Verifying signed \n");

	list($header, $msg) = explode(";", $token);
	list($unused, $sig) = explode(":", $header);

	$resp = callAPI("GET", "auth/publickeys", array());
	if ($resp == false) {
		print("Error calling for public key \n");
		return;
	}

	$success = false;
	$pubKeyObj = json_decode($resp, true);

//	print_r($pubKeyObj);

	foreach ($pubKeyObj["data"] as $pubKeyStr) {
		print_r($pubKeyStr);

		$pubKey = openssl_pkey_get_public($pubKeyStr);
		if ($pubKey == false) {
			printf("unable to decode public key: %s", openssl_error_string());
			return;
		}

		print_r($pubKey);
		print_r(openssl_pkey_get_details($pubKey));

		$algoStr = "sha256";
		if ($algo == OPENSSL_ALGO_SHA384) {
			$algoStr = "sha384";
		} else if ($algo == OPENSSL_ALGO_SHA512) {
			$algoStr = "sha512";
		}

		$err = openssl_verify($msg, base64_decode($sig), $pubKey, $algo);
		if ($err < 0) {
			printf("unable to verify with public key: %s", openssl_error_string());
			return;
		} else if ($err > 0) {
			$success = true;
			break;
		}
	}

	if ($success) {
		print(base64_decode($msg));
	} else {
		print("Invalid token, not verified \n");
	}
}

function callAPI($method, $suburl, $data, $jwt="") {
	global $API_URL;
	global $API_CLIENT_ID;
	global $API_CLIENT_SECRET;

	//open connection
	$ch = curl_init();

	$url = $API_URL . $suburl;

	$headers = array("Content-Type: application/x-www-form-urlencoded", "X-App: test");

	if ($jwt != "") {
		$headers[] = "Authorization: Bearer ". $jwt;
	} else if (!$data || !array_key_exists("client_id", $data)) {
		// $data = $data || array();
		$data["client_id"] = $API_CLIENT_ID;
		$data["client_secret"] = $API_CLIENT_SECRET;
	}

	if ($method == "GET") {
		if (strpos($url, "?") == false) {
			$url = $url . "?";
		}
		$url = $url . "&" . http_build_query($data);
	} else {
		//url-ify the data for the POST
		$data_string = http_build_query($data);
		curl_setopt($ch,CURLOPT_POSTFIELDS, $data_string);
	}

	curl_setopt($ch,CURLOPT_URL, $url);
	curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
	curl_setopt($ch,CURLOPT_POST, $method != "GET");

	//So that curl_exec returns the contents of the cURL; rather than echoing it
	curl_setopt($ch,CURLOPT_RETURNTRANSFER, true);

	//execute post
	$resp = curl_exec($ch);

	return $resp;
}

?>
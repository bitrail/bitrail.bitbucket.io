# Invoicing / QR Code #


BitRail offers the ability to send a request for payment to a customer using a link, or a link embedded as a QR code. This link can be sent to a customer so that the order can be completed on their phone or desktop directly from the link / QR code.

The invoicing process accepts the order details and creates a shortcode for the user to complete the process. Once received, the will be prompted to login (if not already logged in) and complete the checkout process.


## Implementation overview ##

The invoicing mechanism uses a single URL, designed to be reasonably short, to encapsulate the details of the order. These details and associated URL are stored by BitRail for the time period specified when creating the invoice, to allow users time to complete the process.

Once created, the URL can be called directly, generated into a QR image for direct use, or sent to the end user via text or email.

To create this URL, a server-side token must be generated so subsequent calls can be made.

### User Authentication ###

An `access_token` is generated upon user login, and will be required in the calls in this guide. A full guide on generating this JWT/`access_token` can be found in the [OAUTH guide](?file=/oauth/README.md).

### Generate the order URL ###

Creating the URL for the order is performed by making a POST call to `{{BITRAIL_API_URL}}/v1/invoices` with the following parameters and typical values:

**Header Values**

* `Content-Type`: "application/x-www-form-urlencoded"
* `Authorization `: Bearer {access_token} generated from server-side authorization

**POST parameters**

* `dest_vault_id`: Recipient vault_id or handle for the order
* `order_token`: Any token unique to this order. It is used for validating the transaction after completion
* `amount[ordinal]`: Ordinal part of the order amount requested. If the amount is 123.45, the ordinal portion is 123
* `amount[decimal]`: Decimal part of the order amount requested. If the amount is 123.45, the ordinal portion is 45
* `amount[currency]`: Currency part of the order amount requested. If the amount is 123.45FC, the currency would be FreedomCoin. The currency must be supported by this tenant
* `description`: Any alphanumeric description for this transaction. Description may be up to 255 characters.
* `attributes[x][key]` & `attributes[x][value]`: Key/Values for ad-hoc attributes for seller and buyer to see. These may be any key/value pairs, and will be associated with the transaction. Up to 99 attributes allowed. Key length and value length each must be under 255 characters. Keys may not start with an underscore.
* `send_to_email`: (optional) Buyer email address. Invoice email will be sent at time of submission to the user's primary email.
* `send_to_phone`: (optional) Buyer phone number. Invoice text will be sent at time of submission to the user's primary phone number
* `callback_url`: (optional) URL that will be called when client pays this invoice

Example cURL call:

	curl --location --request POST 'https://api.sandbox.bitrail.io/v1/invoices' \
	--header 'Content-Type: application/x-www-form-urlencoded' \
	--header 'Authorization: Bearer {{ACCESS_TOKEN}}' \
	--header 'Accept: application/json' \
	--data-urlencode 'order_token=123456798123456789' \
	--data-urlencode 'dest_vault_id=MyCompanyName' \
	--data-urlencode 'amount[ordinal]=4' \
	--data-urlencode 'amount[decimal]=89' \
	--data-urlencode 'amount[currency]=FC' \
	--data-urlencode 'description=Red Balloon' \
	--data-urlencode 'attributes[0][key]=item_id' \
	--data-urlencode 'attributes[0][value]=1235' \
	--data-urlencode 'attributes[1][key]=sales_tax' \
	--data-urlencode 'attributes[1][value]=0.23' \
	--data-urlencode 'send_to_phone=4045551212' \
	--data-urlencode 'callback_url=http://app.example.com/api/BitrailInvoicePaid'


A successful call will result in a response similar to:

	{
	    "success": true,
	    "data": [
	        {
	            "invoice_id": "3348mpbchl9d9888wlro",
	            "tenant_id": "314p6b7lv40fz5fmy000",
	            "status": "P",
	            "source_vault_id": "32nwdund120igly92pn4",
	            "dest_vault_id": "33461sim31a1emwdylro",
	            "description": "Red Balloon",
	            "send_to_phone": "4045551212",
	            "amount": "4.89",
	            "currency": "FC",
	            "created_at": "2022-05-06T14:34:40.958807303Z",
	            "updated_at": "2022-05-06T14:34:40.958807303Z",
	            "deleted_at": null,
	            "expires_at": "2022-05-21T14:34:40.916165892Z",
	            "client_app_id": "31b0tl72glc02oa76000",
	            "ticket": "921QbTQUxrn",
	            "order_token": "123456798123456789",
	            "callback_url": "http://app.example.com/api/BitrailInvoicePaid",
	            "short_url": "https://pay.sandbox.bitrail.io/921QbTQUxrd",
	            "attributes": [
	                {
	                    "name": "item_id",
	                    "value": "1235"
	                },
	                {
	                    "name": "sales_tax",
	                    "value": "0.23"
	                }
	            ]
	        }
	    ],
	    "meta": {
	        "expires": 1298,
	        "server": "fe80::14ea:ebff:fe80:92b3",
	        "version": "v1.5.61-27a00403(api-qa)"
	    },
	    "paging": null,
	    "errors": null
	}

See Postman documentation for the [invoice endpoint](https://docs.bitrail.io/#b02fc41b-5d80-4947-b5cf-840571919aaf).

The `short_url` result parameter contains the URL suitable for sending to the end user. This can be done manually, sent via one of the `send_to_` paramters in the call, or sent via a subsequent call.

## Sending the invoice ##

The invoice can be sent to the user - either at the time that the invoice URL is generated or afterwards. The functionality is the same, but both methods exist to allow functionality in different situations.


### Send via Text ###

If the `send_to_phone` parameter is specified and the phone number is an active and verified phone number in the BitRail system, then the invoice URL will be generated and sent in a pre-formatted sms text to the user. 

Example text message:

`You have a new invoice on BitRail https://pay.bitrail.io/921QbTQUxrd
`

Contact BitRail to supply custom text for your implementation.

### Send via Email ###

If the `send_to_email` parameter is specified and the email address is an active and verified address in the BitRail system, then the invoice URL will be generated and sent in an email to the user, along with a button linked to the URL.

Contact BitRail to supply custom content for the email

## QR Code generation ##

Once the `short_url` has been generated (sent back in the response from the POST /invoice call), it can be called again to generate a QR code, suitable for display. This can be done by appending either `?qr=png` or `/qr` to the `short_url`.

Example:

`https://pay.sandbox.bitrail.io/921QbTQUxrd/qr`

Will generate a QR code image that may be embedded in an app, web page, kiosk, etc. A customer may then scan this image to be taken directly to pay the invoice (a login may be required first).


## Completion Callback ##

If a `callback_url` is specified when the invoice is generated, then BitRail will make a GET call to this URL when the customer completes payment of the invoice.

* `bitrail_invoice_id`: The invoice ID
* `bitrail_transaction_id`: The ID of the BitRail transaction
* `bitrail_user_id`: The BitRail user ID who paid (might not be source vault user)
* `bitrail_order_token`: The encrypted order token

For the above example invoice, the following would be called when the invoice is paid successfully:

`GET http://app.example.com/api/BitrailInvoicePaid?
 bitrail_invoice_id=3348mpbchl9d9888wlro
 &bitrail_transaction_id=34a0sqmk4fg50f341s56
 &bitrail_user_id=299dnjfs56sn952d576b
 &bitrail_order_token=dh378ch47HHGT_dhccjie754HSggd7363cnj998n235ccnue12caein887ccc8-99cjDHChdhcbAAAccim336bx772MjLSxb73dd
`

The format of the `bitrail_order_token` is discussed in depth on the [JavaScript popup window](?file=/popup_window/README.md).
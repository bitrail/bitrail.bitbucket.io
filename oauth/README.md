
# OAuth2 Authentication



## Introduction

BitRail requires applications use the [OAuth 2.0 Authorization Framework](https://datatracker.ietf.org/doc/html/rfc6749) to securely obtain authorization to BitRail user data. BitRail’s API uses a common implementation of the OAuth 2.0 specification, so any valid Oauth2 library should work. For a list of OAuth 2.0 libraries that should work, see *****. 

There are several authorization flows specific to the OAuth 2.0 protocol. For the best security, BitRail enforces using the Authorization Code Grant Flow with Proof Key for Code Exchange (PKCE) defined by RFC 7636 for most clients applications. PKCE supplements the Authorization Code Grant flow with a dynamically created cryptographically random key (“code verifier”), and its transform value (“code challenge”) to verify the client. Any public client application will get added security with the ability to mitigate authorization code interception attacks. This section will demonstrate how an application obtains the user’s authorization using PKCE.


![](/oauth/general_flow.png)


# Client Application

A client application is any pre-approved application wishing to get a BitRail user’s credentials to perform actions on behalf of the user. For example, a point-of-sale device taking an order or a backoffice application reading the seller’s transactions.


## Setup Client Application

A BitRail representative will create a Client Application record which controls how the application can use the BitRail API and thus how it must authenticate with the API. The result of this setup gives the client application a `client_id` and `client_secret`.


## Login via Authorization Code

### Authorization Request

Applications which need a user’s credentials should use the authorization code method of authentication. When a user visits the application, to start the login process the application will issue a redirect to BitRail’s authorization URL. The URL must contain seven parameters and any additional parameters are ignored. These seven are response_type, scope, state, code_challenge, code_challenge_method, client_id, and redirect_uri.

The `response_type` parameter indicates what kind of response is to be returned. For security reasons, BitRail only accects response_type=code. 

The `scope` lists the items the application would like to access. See the scope documentation for more. 

The `state` parameter is essential to prevent CSRF attacks, so your application can trust that the code sent to it is for the user running the current browser session. The state should be a sufficiently long random value, no more than 255 characters long, unique to this session but could also additionally contain application data needed to tie the user to a request, though sessions are normally used for this. 

The `client_id` parameter is the value assigned to this application.

The `redirect_uri` contains where the authorization will redirect back with the code or error. Note that BitRail will add query parameters code, error, error_description, or error_uri to this call. If the error parameter exists non-empty, then the login failed and the value is an error name. There may be a description or uri accompanying the error but is not meant for end users, but instead for developer use. Otherwise, there will be a code parameter. This code is not valid to identify the user but can only be used to exchange the code for an access_token.

The `code_challenge` parameter, and its companion parameter `code_challenge_method`, are necessary to protect the security of the code exchange. Without this protection, or if you reuse these values, an attacker could trick a user or browser into going to a a link with a stolen code and executing the application as some other user. Always generate a secure random value for each new login session. 

Note: The base64 URL encoding used below is a “Raw URL Encoding”, a simple transform of the standard base 64 encoding by changing “+” to “-” and “/” to “_” and then dropping the padding “=” (see RFC-4648, URL encoding section).

To generate a `code_challenge`, you must first create a cryptographically random value between 43-128 characters long called a `code_verifier`. Typically, this is best achieved by (psuedocode) `code_verifier=base64UrlEncode(randomBytes(48))`.

Then, the `code_challenge` is the `code_verifier`, with a SHA-256 hash and base 64 URL encoded. 

`code_challenge=base64UrlEncode(sha256Hash(code_verifier))`. 

For example, if the code_verifier is `S90nJSvjqMlnJDvw8a2I7Ws_J_uf1UlY76Kfn11PJ9oATHIzJmoVUMz4nPaimXsB` then the `code_challege` will be `hgdQxabJHXJ0o1Lp1Fhh20S6eDs4gamWTnvW-WNoSMQ`

Sample:

```
session_name("MyExampleApp");
session_start();

// The state will be needed for verification later. It protects the caller from forged requests.
$state = base64url_encode(random_bytes(30))
// The code_verifier protects BitRail from forged requests.
// It will be needed for verification later in the call to /v1/auth/token.
$code_verifier = base64url_encode(random_bytes(48));
// The SHA-256 encoded verifier to send in /v1/auth/authorize.
$challenge = base64url_encode(hash("sha256", $code_verifier, true));

// Store the things needed later. Sessions are common but you may want to store elsewhere.
$_SESSION["bitrail_pkce"] = $code_verifier;
$_SESSION["bitrail_state"] = $state;                   
// The redirection URL where the login code or error will be sent.
// This URL base must be registered with BitRail.
//You may add query parameters for redirects or other purposes.
$_SESSION["bitrail_redir"] = "https://example.com/authcode";

// Cause a redirect to happen that launches the login process.
// When it completes, the user is brought back to the redirect_uri.
header("Location: https://api.bitrail.io/v1/auth/authorize?".
  http_build_query(array(
    "response_type"=>"code", 
    "scope"=>"everything",
    "state"=>$state,
    "code_challenge"=>$challenge,
    "code_challenge_method"=>"S256",
    "client_id"=>$clientId,
    "redirect_uri"=>$_SESSION["bitrail_redir"]));
```

Response: 

```
GET /v1/auth/authorize?response_type=code&scope=everything
&state=cBR496af3374293-21bed3fUy5Us9ncmN21s
&code_challenge=hgdQxabJHXJ0o1Lp1Fhh20S6eDs4gamWTnvW-WNoSMQ
&code_challenge_method=S256&client_id=TpzEbST2DNhadI4Q4h6RDALiDnNRKHb2jNThENv_
&redirect_uri=https://app.example.com/auth/code
HTTP/1.1

Host: api.bitrail.io

Location: https://app.bitrail.io/auth/login?......
```


### Access Token Request 

When the oauth2 flow has redirected back to the application with a code or error, the application should show the error or perform the token exchange. The state sent in the authorization code call will be returned as well. The client application should verify that the state matches what was sent.

If the application gets a code parameter, it may exchange it for a token by posting to the token endpoint with grant_type, code, code_verifier, redirect_uri, client_id, and client_secret. Additionally, an optional parameter may be passed to control the type of JSON response to receive.

* The `grant_type` parameter must be authorization_code for this type of exchange.
* The `code` parameter is the code sent to the application in the previous step.
* The `code_verifier` was generated in the previous step to create the challenge.
* The `redirect_uri` must be the same as what was sent in the previous step.
* The `client_id` and `client_secret` were assigned to the application.

If you want the typical Oauth response supported by many libraries, the `wrapped` parameter need not be specified as it is the default response. However, if you wish to get a wrapped response like the other BitRail API calls, send wrapped=1.

 

Authorization code token exchange sample:

```
session_name("MyExampleApp");
session_start();

// The redirection_url may be called if an error occurred. Do not attempt to get a token.
if (isset($_GET["error"]) && $_GET["error"] != "") {
    echo "Show error here. ";
    if (isset($_GET["error_description"])) {
        echo $_GET["error_description"];
    }
    exit();
}

// If a code is received, verify that a state is also received and it matches the state originally sent
if (isset($_GET["code"]) && $_GET["code"] != "") {
    if ($_SESSION["bitrail_state"] != $_GET["state"]) {
        echo "States do not match. Abort!";
        exit();
    }
} else if (!isset($_GET["code"]) || $_GET["code"]=="") {
    echo "Invalid page request.";
    exit();
}

$url = $BITRAIL_API_URL . "v1/auth/token";          // i.e.- https://api.bitrail.io/v1/auth/token
$headers = array("Content-Type: application/x-www-form-urlencoded");
$data = array(
		"grant_type"=>"authorization_code",
		"code" => $_GET["code"],
		"redirect_uri" => $_SESSION["bitrail_redir"],
		"client_id" => $API_CLIENT_ID,
		"client_secret" => $API_CLIENT_SECRET,
		"code_verifier" => $_SESSION["bitrail_pkce"],
		"wrapped" => "1",
		);

$ch = curl_init();
curl_setopt($ch,CURLOPT_POSTFIELDS, http_build_query($data));
curl_setopt($ch,CURLOPT_URL, $url);
curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
curl_setopt($ch,CURLOPT_POST, true);
curl_setopt($ch,CURLOPT_RETURNTRANSFER, true);

$resp = curl_exec($ch);

```
Response:

```
POST /v1/auth/token HTTP/1.1
Host: api.bitrail.io
Content-Type: application/x-www-form-urlencoded
Content-Length: 317

grant_type=authorization_code&code=Rq9LXAR6Sl6zTgB9VxgcNw
&redirect_uri=https%3A%2F%2Fapp.example.com%2Fauth%2Fcode
&client_id=TpzEbST2DNhadI4Q4h6RDALiDnNRKHb2jNThENv_
&client_secret=7i-4TdhmMrpXntPHODAqrSGMXcw-ClJmATohVVYY6iiONlTnsCsYaViib8ZpDMp0
&code_verifier=a1Rtb3NvRHBzT3VyWFpNRHBqbV9DSV9ld0tweEJOcFYzT0szLUdsdHZQQQ

{success: true,
 data: [{
// This should be stored for calling authenticated API calls.
access_token: "eyBza43fh......................",
// This should be stored if you plan to call for a refresh token later.
refresh_token: "eyHs5gu34.....................", 
// This may be used to taylor the UX based on scopes
// the user allowed and is neccessary for calling refresh token.
scope: ".......",
// BitRail user ID logged in
user_id: "229cf8mrw7g43dcm40vn1",
 }],
// Seconds until the token expires though the time is often extended a bit depending on token use.
// All API calls return the current expiration here and /v1/auth/info may be called for a fast look.
meta: {expires: 1798, ...},
 errors: nil}                                       // Any errors here
```




## Login via Client Credentials

An application which is granted the ability to use only their credentials to authenticate must at all times protect the client_id and client_secret. These credentials should never be available to persons except as strictly necessary, nor should they be embedded in a web page, nor should they be sent as URL query parameters, nor should they be sent on a nonsecure HTTP call.

First, the application should POST to `{BITRAIL_API_HOST}/v1/auth/token` with parameters grant_type=client_credentials, scope=everything, redirect_uri=none, client_id, client_secret. See the API documentation for examples. This returns an access token which should be used for authentication to other API calls.


```
POST /v1/auth/token HTTP/1.1
Host: api.bitrail.io
Content-Type: application/x-www-form-urlencoded
Content-Length: 317

grant_type=client_credentials&redirect_uri=none
&client_id=TpzEbST2DNhadI4Q4h6RDALiDnNRKHb2jNThENv_
&client_secret=7i-4TdhmMrpXntPHODAqrSGMXcw-ClJmATohVVYY6iiONlTnsCsYaViib8ZpDMp0&scope=everything
```


## Scopes

Each of the login methods require the client application to specify the scope that it needs to operate. For security it is best to limit scope to that which is strictly required. The API may reduce what scopes are allowed to be requested per application, and users may further restrict the scope.

Scope is sent to the API as a space-delimited list. If an item on the following list has children, then specifying that item will include all of the children also. Scopes are not case sensitive.

| Scope Name |   |                |       |   | Description |
| ---------- | - | -------------- | ----- | - | ----------- |
| Everything |   |                |       |   | All permissions |
| + | Admin |                     |       |   | All admin permissions |
|   | - | Admin.Auth              |       |   | Authorization |
|   | - | Admin.Ach               |       |   | ACH |
|   | - | Admin.Manage            |       |   | Administrative Management |
|   | - | Admin.Users             |       |   | User Administration |
|   | - | Admin.Transactions      |       |   | Transaction Administration |
|   | - | Admin.Vaults            |       |   | Vault Administration |
| + | Standard |                  |       |   | Pseudo-scope holds all items below this line |
| + | User |                      |       | * | All user permissions |
|   | - | User.Profile            |       |   | Name, status, KYC status, verified identity statuses, enabled states includes items permitted to be performed, created / updated / last online dates, emails, phones, and addresses |
|   |   | .                       | Email |   | Primary email and UserID (**Note: This scope cannot be deselected**) |
|   | - | User.PII                |       |   | SSN, DOB |
|   | - | User.Edit               |       |   | Allow editing user information |
|   | - | User.Rewards.Read       |       |   | Read rewards points |
| + | Vault |                     |       | * | All vault permissions |
|   | - | Vault.Edit              |       |   | Allow editing vault details |
|   | - | Vault.Accounts.Balance  |       |   | Allow viewing account balance |
|   | - | Vault.Banks.Edit        |       |   | Allow adding, editing, and delting banks |
|   | - | Vault.Definition        |       |   | View vault, account, and bank account name and IDs |
|   | - | Vault.Profile           |       |   | Allow viewing vault handle and name |
| + | Transaction |               |       | * | All transaction permissions |
|   | - | Transaction.Create      |       |   | Allow creating orders, payments, transfers, etc |
|   | - | Transaction.Refund      |       |   | Allow refunding transactions |
| + | Agreement |                 |       | * | All agreement permissions |
|   | - | Agreement.Read          |       |   | Allow listing and viewing agreements |
|   | - | Agreement.Create        |       |   | Allow creating new agreements |
| + | Invoice |                   |       | * | All Invoice permissions |
|   | - | Invoice.Read            |       |   | Allow listing and viewing invoices |
|   | - | Invoice.Create          |       |   | Allow creating invoices |
| + | History |                   |       | * | All history permissions |
|   | - | History.User            |       |   | View user history |
|   | - | History.Transactions    |       |   | View transaction history |
|   | - | History.Agreements      |       |   | View agreement history |
|   | - | History.Invoices        |       |   | View invoice history |
|   | - | History.Vaults          |       |   | View vault history |
|   | - | History.Vaults.Accounts |       |   | View account history |
|   | - | History.Vaults.Banks    |       |   | View bank history |


# Backend UI and API Interaction


## Setup Backend

Just like any other client application, the authorization UI owned by BitRail will have a client_id and client_secret for use in calling the API. Additionally, the set of endpoints used during the authorization flow must each have a uniquely named URI which is configured within the API. By the API redirecting to well-known URIs and only exchanging cryptographicly opaque keys, the UI can be trusted as our own and valid without needing a secure client_secret for identity.

The UI must not be able to use the client_id and secret for any calls to the API except v1/auth/authorize for the auth flow, and v1/users for user create.

**Auth Flow Pages**

| Name  | Old State  | Proposed URL (https://app.bitrail.io)  | Description  |  
|---|---|---|---|
| login |  | /auth/login | Login |
| signup |  | (not called by API) | Start user signup: username, password |
| mobile | tfa_link | /auth/mobile | Capture phone number |
| redir | tfa_auth | /auth/redir | MNO authenticate by redirect |
| pending | pending | /auth/pending | Wait for TFA |
| otp | (none) | Enter OTP code either from authenticator or sent via SMS. |
| consent | {required consents} | /auth/consent | Get consents, like user agreement. |
| scope | {!bitrail app} | /auth/scope | Ask if a third-party app can have access. |
| setup | {name not confirmed} | /auth/identity | KYC, vault, banking |
| error | Error or fail | /auth/error | Show error which is not allowed on the client application |




## Authorization Flow

After a client application starts the authorization flow, the API will redirect it to the UI one or more times before completing the process. The UI should not assume any particular order. Each call will receive one or more query parameters specified below. All parameters sent MUST be returned to the API on each call to v1/auth/authorize.

**Possible Query Parameters**

| Name | Description |
|------|-------------|
| avi | Authentication Voucher ID - some API calls will take this ID as a bearer token during the signin process. |
| pg | Page called - mirrors the URL path |
| expires | Expiration time in seconds until the pending TFA is invalid. |
| resend | Timeout before resending TFA message is allowed. |
| redir | The TFA URL to call on the redir page. |
| method | The OTP method. |
| tfaphone | The phone mask where TFA was sent. |
| tfaemail | The email mask where TFA was sent. |
| consents | The list of consents of which the user must agree. |
| msg | An error message to show, like an invalid password |
| kyc | The user’s KYC status. |
| scopes | The space-delimited scopes the application is requesting. |
| email | The primary email of the person who has begun the login process. |
| uid | The user ID of the person who has begun the login process. |
| client_name | The descriptive name of the client application asking for authorization |


### Login Page

The login page is called with no special parameters except for API use. This page may allow a user to retrieve a forgotten password or to signup (see **Signup Page**).

When the user submits their username and password, those fields must be posted to auth/authorize.


### Signup Page

The signup page operates similar to login except it first calls the API to create the user. The page should show and ask for any required consent needed. The consents parameter will contain any consents needed in a comma-separated list. The create user call must be passed the email, password, and avi body parameters. 

After create user succeeds, the page calls a post to auth/authorize with body parameters username, password, and a consent:{name}=1 for each required consent for which the user agreed.


### Mobile Page

If the API wishes to send a TFA but does not have enough device information, this page will obtain the mobile number of the user. The tfaprompt parameter will contain “force” or “ask” to determine if the user must enter the number to continue or not.

The user submits their number (or not) by posting to auth/authorize with the mobile body parameter so as to be redirected to the next step. If the user is skipping to add a mobile device, submit with this value empty.


### Redir Page

When the API recognizes that a user’s device is connected to a supported mobile carrier, TFA is attempted by sending a message to the carrier. The UI should attempt to call the URL specified in the redir parameter. Once that call has had a chance to complete, the UI should submit the auth/authorize to be redirected to the next step. If an error occurs in the UI or attempting to call the redir, the UI may go ahead and submit auth/authorize.


### Pending Page

This page will be called when the API is waiting for the TFA to complete. It can use the tfaphone, tfaemail, and expires parameters to show the user information. If for some reason no expiration is sent, assume ten minutes. The page should occasionally call the `v1/auth/info` endpoint, using the avi parameter as the Authorization Bearer token, to get the current status. If the time expires or status becomes something other than tfa_link, tfa_auth, or pending, it should submit to `v1/auth/authorize` to be redirected to the next step.


### OTP Page

Whenever possible, the API will attempt an OTP login. Some users will have a more secure authenticator app configured while others will recieve a text message and/or email containing a temporary code. The parameter 'method' will be one of sms, email, sms/email, or totp. The page messaging should let the user know where to look for the code. The entered code length must be 6,7, or 8 digits long. The 6 digit codes may start with a '0' but the longer ones must not start with a '0'. The entered code is submitted to the API in parameter 'otp'. Once the resend timeout expires, the user could be permitted to ask for the email/sms to be resent. The app may call `v1/auth/link/resend` via POST to ask for the resend to occur. Assuming the avi is used as a token, a not_authorized error typically means the login window has expired.


### Consent Page

The consent page is called when the user needs to consent to one or more items listed in the consents parameter. The UI must collect these consents. The user is not required to consent to them all to continue, but the API will likely send an error message to the original client explaining why access has not been granted.


### Setup Page

The setup page step is all pages necessary to complete the user signup process. At this time this includes gathering KYC information, setting up the user vault, and adding bank account(s). The calls necessary to complete these tasks may use the AVI as the bearer token. For security, the AVI can only be used during login for a limited set of calls with limited functionality. The uid parameter contains the user ID needed to get fundamental data. 

The UI can call:

* `GET|PUT v1/users/{USER_ID}` to get a very reduced set of data
* `GET|POST v1/users/{USER_ID}/verify` and subpaths for KYC processing
* `GET|POST v1/users/{USER_ID}/banking` and subpaths 
* `GET v1/users/{USER_ID}/vaults` 
* `PUT v1/vaults/{VAULT_ID}` 
* `GET|POST|PUT|DELETE v1/vaults/{VAULT_ID}/bank_accounts` and subpaths

The kyc parameter contains a ‘1’ or ‘0’. If it is ‘1’, the user should be asked for KYC information.

Once the UI is done gathering setup information, it must submit for redirect to v1/auth/authorize.


### Scopes Page

When the client application is not a BitRail controlled application, the API will ask the user if it is okay to share information with the client application and allow the user to narrow the scope of data that could be shared.

The scopes parameter will contain the space delimited list of scopes that the application has requested. The user should be able to select/de-select any scope except `email`. The UI must present an Allow and Deny buttons to allow the selected scopes or deny all. Upon selecting Allow or Deny, the UI will submit for redirect the button pushed and the scopes selected.

The parameter for Allow is `allow=allow` and the parameter for the button Deny is `deny=deny`.

The parameter(s) for selected scopes are in the form `allowed_scopes=a_scope_name&allowed_scopes=another_scope_name`, etc. Do not send any scope not selected, or if using checkboxes on a form submit (where all are named `allowed_scopes`, send the default value “off”, like `allowed_scopes=off`. Alternatively, the `allowed_scopes` parameter can be a space-delimited set of allowed scopes, like `allowed_scopes=a_scope_name+another_scope_name` (using the plus sign as a URL encoded space, though %20 will also work).


### Error Page

The Oauth 2.0 standard dictates some errors which we must show and not send to the client application. There are a few fundamental errors which will also get shown here. The standard specifies the parameters error and error_description parameters where error is the type of error. The error types are:

* `server_error`
* `access_denied`
* `invalid_request`
* `unauthorized_client`
* `unsupported_response_type`
* `invalid_scope`
* `temporarily_unavailable`
* `unsupported_grant_type`
* `invalid_grant`
* `invalid_client`

The `error_description` contains developer text for debugging so the UI is encouraged to use pre-built messages for each of the types above.


## Visual Flow Diagram


![](/oauth/br_oauth2_flow.png)


# Authentication using Client Credentials

## Introduction

The BitRail platform is being transitioned to use a full OAUTH2 implementation model. See the [OAUTH guide](?file=/oauth/README.md) for more details.

For now, a simpler client credentials call can be implemented for some scenarios. 



## Implementation

A JWT token / access_token **must be** generated server-side and passed to the front-end UI to ensure the *clientID* and *secret* used are not exposed to the public.

A POST call is made to `{{BITRAIL_API_URL}}/v1/auth/token` with the following parameters and typical values:

* `grant_type`: "client_credentials"
* `redirect_uri`: "none"
* `scope`: "everything"
* `client_id`: *<BitRail supplied id with server permissions>*
* `client_secret `: *<BitRail supplied secret with server permissions>*

Example cURL call:

	curl --location --request POST 'https://api.sandbox.bitrail.io/v1/auth/token' \
		--header 'Cache-Control: no-cache' \
		--header 'Content-Type: application/x-www-form-urlencoded' \
		--data-urlencode 'grant_type=client_credentials' \
		--data-urlencode 'client_id=***' \
		--data-urlencode 'client_secret=***' \
		--data-urlencode 'redirect_uri=none' \
		--data-urlencode 'scope=everything'

A successful call will result in a response similar to:

	{
	    "success": true,
	    "data": [
	        {
	            "access_token": "***",
	            "expires_in": 3600,
	            "scope": "everything",
	            "state": "client",
	            "token_type": "Bearer"
	        }
	    ],
	    "meta": {
	        "expires": 3600,
	        "server": "fe80::87f:ddff:fe7f:d817",
	        "version": "v1.4.101-2ddc5a81"
	    },
	    "paging": null,
	    "errors": null
	}

View full Postman documentation for [BitRail authentication](https://docs.bitrail.io/#8d292e65-1e20-41f5-9511-50ed60d8b978) and more specifically [service application token request](https://docs.bitrail.io/#bb068997-3fdd-48c1-9851-bb91114ceb20).

The result will include the JWT in the `access_token` field, and will be valid for the number of seconds in `expires`. Tokens can (and should, if possible) be cached for up to the `expires` timeframe, to reduce unnecessary server load.
# BitRail Vendor Implementation Guide

This guide is intended to assist a developer integrate BitRail into a 3rd party vendor's checkout process or supplement a vendor's payment processing options. It is intended as a starting point for adding BitRail as a payment processing option.

## Core Concepts

![](concepts/api_concepts_sm.png)

A fundamental understanding of how objects and concept relate to one another in the BitRail system is key to understanding how both the API works and how BitRail works within a client's system.

The [core concepts](?file=/concepts/README.md) guide walks through each of the main topics and describes their typical use.


## Integration

### Full Integration

A full integration with BitRail entails implementing the [REST API](https://docs.bitrail.io/) so that the [authorization](?file=/oauth/README.md), identity verification, transactions, invoicing, and any other required functionality is added directly into your product.

While is this is the most difficult approach, it provides the best user experience to your own customers.

### Hybrid Integration

Because the programming required to support some of BitRail's core functionality can be fairly complex, BitRail can operate in a hybrid model. In this model, you may implement any portion that meets the needs of your business, and allow BitRail's UI handle the rest. 

There are a number of ways to approach this, but the most common are using a custom BitRail UI wallet (so the user can login, KYC, associate bank accounts, etc) within the wallet, but the checkout process can be handled by either handing over control to a popup window for completion of the checkout process or by providing the user with an invoice (either by email or text). 

#### Popup Window

The quickest and easiest way to integrate BitRail into your checkout process is to use a [JavaScript popup window](?file=/popup_window/README.md). This requires minimal server-side and client-side work, and allows you to add BitRail as a checkout method to your website with minimal effort.

#### Invoicing

Documentation for BitRail [invoicing and QR code](?file=/invoicing/README.md) is available to implement a URL for a direct checkout. This URL can be used as a QR code and/or for sending to a customer via text or email.
	
## Authentication Overview

The majority of the calls on the BitRail platform are secure calls, requiring authentication. To achieve this, BitRail uses OAUTH in order to generate an access_token used during that session.

Because this authentication process is somewhat complex and required for all functions, a full guide is available in the [OAUTH guide](?file=/oauth/README.md).


## Full REST API Documentation

A [full REST API](https://docs.bitrail.io/) is available for full integration. Postman is used to document and execute queries against the API. The documentation can be loaded into Postman for testing the endpoints in your own environment. The documentation gives examples of both execution of the endpoints in many different languages, and most endpoints have example responses to better show what can be expected.


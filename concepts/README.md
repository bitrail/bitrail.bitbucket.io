# Core Concepts

![](/concepts/api_concepts.png)

### Tenant

A tenant generally represents a BitRail customer. This ensures that a client has vaults, currencies, customizations, and other items specific to its needs.

### Client App

A client app represents a specific application for a tenant. A client app may be a web page for a wallet, an mobile app, a backend server process, or some other application that has specific needs for the tenant. 

Each client app may have its own allowable permissions, and may have a completely different look and feel, but be associated with the same tenant.

### Currency

A currency is simply a stable coin represented in the system. A tenant may have one or more associated currencies that are allowed to be transacted upon, and would be available for the tenant's users.

### Customization

Each tenant will have custom emails, texts, and other messaging specific to the associated client or company. These customizations are associated a tenant, so that if and end user interacts specific client app, they'll always receive the appropriate customized messaging for that tenant.

### Rules

Each tenant has rules associated with it to govern which actions are allowed. These rules my include the ability to send money between users, limits on how much money can be transacted, and other tenant-specific allowances.

### Vaults

The "wallet" that is held by a user called a `vault` in the BitRail API. Currently a wallet is associated with one user, although this is may be expanded in the future. The wallet contains accounts, bank accounts, and the associated transactions. 

Refer to the the [Vaults documentation](https://docs.bitrail.io/#9e48aeb1-abd6-4260-9cb7-1efc5746f4a1) for more information.

### Users

A user represents an entity using the system - typically a person or a business entity. A user is currently associated with one vault. A user may have an associated phone, email, or address, and have a verified identity. 

Refer to the the [Users documentation](https://docs.bitrail.io/#7c2cd54c-45dd-4da2-a286-876c7fb97756) for more information.

### Accounts

A vault may have one or more account within it, representing the value held in a specific currency. An account will always have an associated balance (even if that balance is zero).

Refer to the the [Accounts documentation](https://docs.bitrail.io/#22e09091-8207-4a48-aa34-6082219f2633) for more information.

### Bank Accounts

A bank account represents a real bank account. The bank account is used anytime the user moves money in or out of the system, using ACH.

Refer to the the [Bank Accounts documentation](https://docs.bitrail.io/#19538754-08d9-4480-aba7-6423706f83b7) for more information.


### Transactions

Transactions are any movement of funds - whether between the same user's account or sending funds to another user (a transfer or an order).

Refer to the the [Transactions documentation](https://docs.bitrail.io/#c1b74eec-4c5c-4e81-9922-ffd6d68311b4) for more information.

### Phone

The phone is central part of BitRail's identity verification and two factor authentication mechanism. Because a phone number is associated with a single user, duplicate phone numbers are not allowed per tenant.

Refer to the the [Phone documentation](https://docs.bitrail.io/#dca1ceb3-6ade-41ad-9b02-3644be80fb19) for more information.

### Email

Invoices, transaction receipts, notices, and other messaging is delivered to users by email. It's possible for multiple emails to be associated with a user.

Refer to the the [Email documentation](https://docs.bitrail.io/#31323b92-2714-4266-b5fb-d7615ab05841) for more information.

### Address

The physical address of a user is required as part of identity verification, since BitRail's regulatory requirement requires a verified physical address.

Refer to the the [Address documentation](https://docs.bitrail.io/#b5728372-3675-4ee2-ba09-03ad721d1dc0) for more information.
